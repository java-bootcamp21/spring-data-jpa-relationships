package com.jpa.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQuery;

@Entity
public class Registration extends BaseEntity<Integer>{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	
    public Registration() {
		super();
	}

	public Registration(Tournament tournament, Player player) {
		super();
		this.tournament = tournament;
		this.player = player;
	}

	@ManyToOne
    @JoinColumn(name = "tournament_id",referencedColumnName = "id")
    private Tournament tournament;
    
    @ManyToOne
    @JoinColumn(name = "player_id",referencedColumnName = "id")
    private Player player;

    
    public Tournament getTournament() {
		return tournament;
	}

	public void setTournament(Tournament tournament) {
		this.tournament = tournament;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	@Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Registration{" +
                "id=" + id +
                '}';
    }
}
