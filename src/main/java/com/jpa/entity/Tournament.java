package com.jpa.entity;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;

@Entity
public class Tournament extends BaseEntity<Integer> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String location;
    
    public Tournament() {
		super();
	}


	public Tournament(String name, String location) {
		super();
		this.name = name;
		this.location = location;
	}

	@OneToMany(mappedBy = "tournament",cascade = CascadeType.ALL)
    private List<Registration> registrations= new ArrayList<>();
    
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
    		name = "tournament_category",
    		joinColumns = @JoinColumn(name = "tournament_id"),
    		inverseJoinColumns = @JoinColumn(name = "category_id")
    		)
    private List<Category> categories = new ArrayList<>();
    
    

    public List<Category> getCategories() {
		return categories;
	}


	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}


	public List<Registration> getRegistrations() {
		return registrations;
	}
    

	public void setRegistrations(List<Registration> registrations) {
		this.registrations = registrations;
	}

	@Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Tournament{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                '}';
    }

}
