package com.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jpa.entity.Registration;

@Repository
public interface RegistrationRepository extends JpaRepository<Registration,Integer> {
	
	Registration findFirstByIdAndPlayer_id(Integer id, Integer playerId);

}
