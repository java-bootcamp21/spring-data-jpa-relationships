package com.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jpa.entity.PlayerProfile;

@Repository
public interface PlayerProfileRepository extends JpaRepository<PlayerProfile,Integer> {



}
