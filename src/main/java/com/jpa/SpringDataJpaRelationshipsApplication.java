package com.jpa;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

import com.jpa.dto.CategoryDto;
import com.jpa.dto.PlayerDto;
import com.jpa.dto.TournamentDto;
import com.jpa.entity.Registration;
import com.jpa.entity.Tournament;
import com.jpa.repository.CategoryRepository;
import com.jpa.repository.PlayerRepository;
import com.jpa.repository.RegistrationRepository;
import com.jpa.repository.TournamentRepository;
import com.jpa.service.CategoryService;
import com.jpa.service.PlayerService;
import com.jpa.service.RegistrationService;
import com.jpa.service.TournamentService;

@SpringBootApplication
public class SpringDataJpaRelationshipsApplication implements CommandLineRunner {

	private final Logger logger = LoggerFactory.getLogger(SpringDataJpaRelationshipsApplication.class);
	
	@Autowired
	private PlayerRepository playerRepository;
	
	@Autowired
	private TournamentRepository tourRepository;
	
	@Autowired 
	private CategoryRepository catReposiroRepository;
	
	@Autowired
	private RegistrationRepository registrationRepository;
	
	@Autowired
	private PlayerService playerService;
	
	@Autowired
	private TournamentService tourService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private RegistrationService registrationService;
	
	
    public static void main(String[] args) {
        SpringApplication.run(SpringDataJpaRelationshipsApplication.class, args);
    }

    @Transactional
	@Override
	public void run(String... args) throws Exception {
		cleanDatabase();
		
		//add player 
		var player1 = new PlayerDto("player 1", "profile 1");
		var player2 = new PlayerDto("player 2", "profile 2");
		var player3 = new PlayerDto("player 3", "profile 3");
		var player4 = new PlayerDto("player 4", "profile 4");
		
		player1 = playerService.addPlayer(player1);
		player2 = playerService.addPlayer(player2);
		player3 = playerService.addPlayer(player3);
		player4 = playerService.addPlayer(player4);
		
		// add tournament category
		
		var category1 = new CategoryDto("Tournament Category 1");
		var category2 = new CategoryDto("Tournament Category 2");
		
		category1 = categoryService.addCategory(category1);
		category2 = categoryService.addCategory(category2);
		
		//add tournament
		var tournament1 = new TournamentDto(null,"Tournament 1", "Albania");
		var tournament2 = new TournamentDto(null,"Tournament 2", "Albania");
		tournament1 = tourService.addTournament(tournament1);
		tournament2 = tourService.addTournament(tournament2);
		
		//add tournament category 
		var categoryToAdd1 = categoryService
				.findCategoryById(category1.getCategoryId());
		var categoryToAdd2 = categoryService
				.findCategoryById(category2.getCategoryId());
		tournament1 = tourService
				.addTournamentCategory(tournament1.getTournamentId(), categoryToAdd1);
		tournament2 = tourService
				.addTournamentCategory(tournament2.getTournamentId(), categoryToAdd2);
		
//		add registrations
		var playerToReg1 = playerService.findById(player1.getPlayerId());
		var tournamentToReg1 = tourService.findTournament(tournament1.getTournamentId());
		var reg1 = registrationService.addRegistration(playerToReg1,tournamentToReg1);
		
		var playerToReg2 = playerService.findById(player2.getPlayerId());
		var tournamentToReg2 = tourService.findTournament(tournament1.getTournamentId());
		var reg2 = registrationService.addRegistration(playerToReg2,tournamentToReg2);
		
		var playerToReg3 = playerService.findById(player3.getPlayerId());
		var tournamentToReg3 = tourService.findTournament(tournament2.getTournamentId());
		var reg3 = registrationService.addRegistration(playerToReg3,tournamentToReg3);
		
		var playerToReg4 = playerService.findById(player4.getPlayerId());
		var tournamentToReg4 = tourService.findTournament(tournament2.getTournamentId());
		var reg4 = registrationService.addRegistration(playerToReg4,tournamentToReg4);
		
		logger.info("Available Players are {}",Arrays.asList(player1,player2,player3,player4));
		logger.info("Available Tournament Categories are {}",Arrays.asList(category1,category2));
		logger.info("Available Tournament are {}",Arrays.asList(tournament1,tournament2));
		logger.info("Available Tournament Registration are {}",Arrays.asList(reg1,reg2,reg3,reg4));
		
		Registration result = registrationRepository.findFirstByIdAndPlayer_id(reg4.getRegistrationId() ,playerToReg4.getId());
		logger.info("Registration where regId {} and playerId {} --- result {}",reg4.getRegistrationId() ,playerToReg4.getId(),result);
		
		logger.info("Tournament by categoryId {}--- result {}",category1.getCategoryId(),tourRepository.findAllByCategoriesIdAndCategoriesName(category1.getCategoryId(),category1.getCategoryName()));
		
		logger.info("User List {}",playerRepository.findAll());
		
		
		
	}
	
	private void cleanDatabase() {
		playerRepository.deleteAll();
		tourRepository.deleteAll();
		catReposiroRepository.deleteAll();
		
	}

}
