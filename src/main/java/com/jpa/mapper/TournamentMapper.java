package com.jpa.mapper;

import com.jpa.dto.TournamentDto;
import com.jpa.entity.Tournament;

public class TournamentMapper {
	
	public static Tournament toEntity(TournamentDto t) {
		return new Tournament(t.getName(),t.getLocation());
	}
	
	public static TournamentDto toDto(Tournament t) {
		return new TournamentDto(t.getId(), t.getName(),t.getLocation());
	}

}
