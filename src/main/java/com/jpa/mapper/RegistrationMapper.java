package com.jpa.mapper;

import com.jpa.dto.RegistrationDto;
import com.jpa.entity.Registration;

public class RegistrationMapper {
	
	public static RegistrationDto toDto(Registration reg) {
		return new RegistrationDto(reg.getId(), reg.getPlayer().getId(),
						reg.getPlayer().getName(), 
						reg.getTournament().getId(), 
						reg.getTournament().getName());
	}

}
