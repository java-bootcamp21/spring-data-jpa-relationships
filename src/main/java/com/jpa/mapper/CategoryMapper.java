package com.jpa.mapper;

import com.jpa.dto.CategoryDto;
import com.jpa.entity.Category;

public class CategoryMapper {
	
	public static Category toEntity(CategoryDto c) {
		return new Category(c.getCategoryName());
	}
	
	public static CategoryDto toDto(Category c) {
		return new CategoryDto(c.getId(), c.getName());
	}
	

}
