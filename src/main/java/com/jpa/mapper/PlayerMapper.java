package com.jpa.mapper;

import com.jpa.dto.PlayerDto;
import com.jpa.entity.Player;
import com.jpa.entity.PlayerProfile;

public class PlayerMapper {
	
	public static PlayerDto toDto(Player p) {
		return new PlayerDto(p.getId(), p.getName(), p.getPlayerProfile()
				.getTwitter());
	}
	
	public static Player toEntity(PlayerDto p) {
		var player = new Player();
		player.setName(p.getPlayerName());
		player.setPlayerProfile(p.getPlayerProfile()!=null?new PlayerProfile(p.getPlayerProfile()):null);
		return player;
	}

}
