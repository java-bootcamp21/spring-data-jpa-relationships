package com.jpa.dto;

public class TournamentDto {
	
	private Integer tournamentId;
	private String name;
	private String location;
	
	
	
	public TournamentDto(Integer tournamentId, String name, String location) {
		super();
		this.tournamentId = tournamentId;
		this.name = name;
		this.location = location;
	}
	
	
	
	public TournamentDto() {
		super();
	}



	public Integer getTournamentId() {
		return tournamentId;
	}
	public void setTournamentId(Integer tournamentId) {
		this.tournamentId = tournamentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}



	@Override
	public String toString() {
		return "TournamentDto [tournamentId=" + tournamentId + ", name=" + name + ", location=" + location + "]";
	}
	
	

}
