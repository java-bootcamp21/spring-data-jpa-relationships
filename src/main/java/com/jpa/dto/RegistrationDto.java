package com.jpa.dto;

public class RegistrationDto {
	private Integer registrationId;
	private Integer playerId;
	private String playerName;
	private Integer tournamentId;
	private String tounamentName;
	
	
	
	public RegistrationDto() {
		super();
	}



	public RegistrationDto(Integer registrationId, Integer playerId, String playerName, Integer tournamentId,
			String tounamentName) {
		super();
		this.registrationId = registrationId;
		this.playerId = playerId;
		this.playerName = playerName;
		this.tournamentId = tournamentId;
		this.tounamentName = tounamentName;
	}



	public Integer getRegistrationId() {
		return registrationId;
	}



	public void setRegistrationId(Integer registrationId) {
		this.registrationId = registrationId;
	}



	public Integer getPlayerId() {
		return playerId;
	}



	public void setPlayerId(Integer playerId) {
		this.playerId = playerId;
	}



	public String getPlayerName() {
		return playerName;
	}



	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}



	public Integer getTournamentId() {
		return tournamentId;
	}



	public void setTournamentId(Integer tournamentId) {
		this.tournamentId = tournamentId;
	}



	public String getTounamentName() {
		return tounamentName;
	}



	public void setTounamentName(String tounamentName) {
		this.tounamentName = tounamentName;
	}



	@Override
	public String toString() {
		return "RegistrationDto [registrationId=" + registrationId + ", playerId=" + playerId + ", playerName="
				+ playerName + ", tournamentId=" + tournamentId + ", tounamentName=" + tounamentName + "]";
	}
	
	
	
	
	
	

}
