package com.jpa.service;

import java.util.List;

import com.jpa.dto.PlayerDto;
import com.jpa.entity.Player;

public interface PlayerService {
	
	PlayerDto addPlayer(PlayerDto player);
	Player findById(Integer id);
	List<PlayerDto> listPlayers();
	void deletePlayer(Integer id);
	PlayerDto addPlayerProfile(Integer playerId, String profile);
	
}
