package com.jpa.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.jpa.dto.PlayerDto;
import com.jpa.entity.Player;
import com.jpa.entity.PlayerProfile;
import com.jpa.mapper.PlayerMapper;
import com.jpa.repository.PlayerRepository;
import com.jpa.service.PlayerService;

import static com.jpa.mapper.PlayerMapper.*;

@Service
public class PlayerServiceImpl implements PlayerService{
	
	private final PlayerRepository playerRepository;
	
	public PlayerServiceImpl(PlayerRepository playerRepository) {
		this.playerRepository = playerRepository;
	}

	@Override
	public PlayerDto addPlayer(PlayerDto player) {
		var playerToAdd = toEntity(player);
		return toDto(playerRepository.save(playerToAdd));
	}

	@Override
	public Player findById(Integer id) {
		return playerRepository.findById(id)
				.orElseThrow(()-> new RuntimeException(String
						.format("player with id %s not found", id)));
	}

	@Override
	public List<PlayerDto> listPlayers() {
		return playerRepository.findAll().stream()
				.map(p -> toDto(p))
				.collect(Collectors.toList());
	}

	@Override
	public void deletePlayer(Integer id) {
		playerRepository.findById(id)
				.ifPresentOrElse(p -> playerRepository.delete(p)
						, () -> new RuntimeException(String
								.format("cannot delete player! Player with id %s does not exist", id)));;
		
	}

	@Override
	public PlayerDto addPlayerProfile(Integer playerId, String profile) {
		var player = findById(playerId);
		player.setPlayerProfile(new PlayerProfile(profile));
		return toDto(playerRepository.save(player));
	}


}
