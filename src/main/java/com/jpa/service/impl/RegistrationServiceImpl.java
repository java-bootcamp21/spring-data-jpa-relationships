package com.jpa.service.impl;

import org.springframework.stereotype.Service;

import com.jpa.dto.RegistrationDto;
import com.jpa.entity.Player;
import com.jpa.entity.Registration;
import com.jpa.entity.Tournament;
import com.jpa.repository.RegistrationRepository;
import com.jpa.service.RegistrationService;

import static com.jpa.mapper.RegistrationMapper.*;

@Service
public class RegistrationServiceImpl implements RegistrationService{

	private final RegistrationRepository regRepository;
	
	
	public RegistrationServiceImpl(RegistrationRepository regRepository) {
		super();
		this.regRepository = regRepository;
	}

	@Override
	public RegistrationDto addRegistration(Player player, Tournament tournament) {
		var reg = new Registration(tournament,player);
		return toDto(regRepository.save(reg));
	}

	@Override
	public void deleteRegistration(Integer id) {
		var registrationToDelete = findRegistrationById(id);
		regRepository.delete(registrationToDelete);
	}

	@Override
	public Registration findRegistrationById(Integer id) {
		return regRepository.findById(id)
				.orElseThrow(()-> new RuntimeException(String
						.format("Registration with id %s not found", id)));
	}

	
}
