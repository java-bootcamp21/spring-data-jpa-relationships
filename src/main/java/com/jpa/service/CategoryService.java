package com.jpa.service;

import java.util.List;

import com.jpa.dto.CategoryDto;
import com.jpa.entity.Category;

public interface CategoryService {
	
	Category findCategoryById(Integer categoryId);
	CategoryDto addCategory(CategoryDto c);
	CategoryDto getCategory(Integer categoryId);
	List<CategoryDto> getCategories();
}
