package com.jpa.service;

import java.util.List;

import com.jpa.dto.TournamentDto;
import com.jpa.entity.Category;
import com.jpa.entity.Tournament;

public interface TournamentService {
	
	Tournament findTournament(Integer id);
	TournamentDto addTournament(TournamentDto tournament);
	List<TournamentDto> listTournament();
	TournamentDto addTournamentCategory(Integer tounamentId,Category category);
	TournamentDto removeCategory(Integer tounamentId,Integer categoryId);
	void deleteTournamet(Integer tournamentId);
	
}
