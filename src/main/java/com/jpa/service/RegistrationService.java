package com.jpa.service;

import com.jpa.dto.RegistrationDto;
import com.jpa.entity.Player;
import com.jpa.entity.Tournament;
import com.jpa.entity.Registration;

public interface RegistrationService {
	
	RegistrationDto addRegistration(Player player, Tournament tournament);
	Registration findRegistrationById(Integer id);
	void deleteRegistration(Integer id);

}
